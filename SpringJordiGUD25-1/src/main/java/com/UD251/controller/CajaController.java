package com.UD251.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.UD251.dto.Caja;
import com.UD251.service.CajaService;


@RestController
@RequestMapping("/api")
public class CajaController {
	@Autowired
	CajaService cajaService;
	
	@GetMapping("/cajas")
	public List<Caja> listarCajas(){
		return cajaService.listarCajas();
	}
	
	@PostMapping("/cajas")
	public Caja salvarCaja(@RequestBody Caja caja) {
		
		return cajaService.guardarCaja(caja);
	}
	
	@GetMapping("/cajas/{num_referencia}")
	public Caja videoXID(@PathVariable(name="num_referencia")String num_referencia) {
		
		Caja caja_xid= new Caja();
		
		caja_xid=cajaService.cajaXID(num_referencia);
		
		System.out.println("Video XID: "+caja_xid);
		
		return caja_xid;
	}
	
	@PutMapping("/cajas/{num_referencia}")
	public Caja actualizarCaja(@PathVariable(name="num_referencia")String num_referencia,@RequestBody Caja caja) {
		
		Caja caja_seleccionado= new Caja();
		Caja caja_actualizado= new Caja();
		
		caja_seleccionado= cajaService.cajaXID(num_referencia);
		
		caja_seleccionado.setNumReferencia(caja.getNumReferencia());
		caja_seleccionado.setContenido(caja.getContenido());
		caja_seleccionado.setValor(caja.getValor());
		caja_seleccionado.setAlmacen(caja.getAlmacen());
		
		caja_actualizado = cajaService.actualizarCaja(caja_seleccionado);
		
		System.out.println("El video actualizado es: "+ caja_actualizado);
		
		return caja_actualizado;
	}
	
	@DeleteMapping("/cajas/{num_referencia}")
	public void eliminarCaja(@PathVariable(name="num_referencia")String num_referencia) {
		cajaService.eliminarCaja(num_referencia);
	}
}
