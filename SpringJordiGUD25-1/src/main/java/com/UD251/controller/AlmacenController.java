package com.UD251.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.UD251.dto.Almacen;
import com.UD251.service.AlmacenService;


@RestController
@RequestMapping("/api")

public class AlmacenController {
	@Autowired
	AlmacenService almacenService;

	@GetMapping("/almacenes")
	public List<Almacen> listarAlmacenes(){
		return almacenService.listarAlmacenes();
	}
	
	@PostMapping("/almacenes")
	public Almacen salvarAlmacen(@RequestBody Almacen almacen) {
		
		return almacenService.guardarAlmacen(almacen);
	}
	
	@GetMapping("/almacenes/{id}")
	public Almacen almacenXID(@PathVariable(name="id") Long id) {
		
		Almacen almacen_xid= new Almacen();
		
		almacen_xid=almacenService.AlmacenXID(id);
		
		System.out.println("Almacen XID: "+almacen_xid);
		
		return almacen_xid;
	}
	
	@PutMapping("/almacenes/{id}")
	public Almacen actualizarCliente(@PathVariable(name="id")Long id,@RequestBody Almacen almacen) {
		
		Almacen almacen_seleccionado= new Almacen();
		Almacen almacen_actualizado= new Almacen();
		
		almacen_seleccionado= almacenService.AlmacenXID(id);
		
		almacen_seleccionado.setLugar(almacen.getLugar());
		almacen_seleccionado.setCapacidad(almacen.getCapacidad());

		
		almacen_actualizado = almacenService.actualizarAlmacen(almacen_seleccionado);
		
		System.out.println("El cliente actualizado es: "+ almacen_actualizado);
		
		return almacen_actualizado;
	}
	
	/*@DeleteMapping("/almacenes/{id}")
	public void eliminarAlmacen(@PathVariable(name="id")Long id) {
		almacenService.eliminarAlmacen(id);
	}*/
	@DeleteMapping("/almacenes/{id}")
	public void eliminarAlmacen(@PathVariable(name="id")Long id) {
		almacenService.eliminarAlmacen(id);
	}
}
