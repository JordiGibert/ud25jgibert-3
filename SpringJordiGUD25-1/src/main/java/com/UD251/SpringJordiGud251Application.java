package com.UD251;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJordiGud251Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringJordiGud251Application.class, args);
	}

}
