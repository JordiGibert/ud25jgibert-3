package com.UD251.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.UD251.dao.ICajaDAO;
import com.UD251.dto.Caja;

@Service
public class CajaService implements ICajaService{

	@Autowired
	ICajaDAO iCajaDAO;
	
	@Override
	public List<Caja> listarCajas() {
		// TODO Auto-generated method stub
		return iCajaDAO.findAll();
	}

	@Override
	public Caja guardarCaja(Caja caja) {
		// TODO Auto-generated method stub
		return iCajaDAO.save(caja);
	}

	@Override
	public Caja cajaXID(String num_referencia) {
		// TODO Auto-generated method stub
		return iCajaDAO.findById(num_referencia).get();
	}

	@Override
	public Caja actualizarCaja(Caja caja) {
		// TODO Auto-generated method stub
		return iCajaDAO.save(caja);
	}

	@Override
	public void eliminarCaja(String num_referencia) {
		// TODO Auto-generated method stub
		iCajaDAO.deleteById(num_referencia);
		
	}

}
