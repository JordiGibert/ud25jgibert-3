package com.UD251.service;

import java.util.List;

import com.UD251.dto.Almacen;



public interface IAlmacenService {
	//Metodos del CRUD
		public List<Almacen> listarAlmacenes(); //Listar All 
		
		public Almacen guardarAlmacen(Almacen almacen);	//Guarda un cliente CREATE
		
		public Almacen AlmacenXID(Long id); //Leer datos de un cliente READ
		
		public Almacen actualizarAlmacen(Almacen almacen); //Actualiza datos del cliente UPDATE
		
		public void eliminarAlmacen(Long id);// Elimina el cliente DELETE
}
