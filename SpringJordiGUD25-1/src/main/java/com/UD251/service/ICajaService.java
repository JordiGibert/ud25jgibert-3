package com.UD251.service;

import java.util.List;

import com.UD251.dto.Caja;



public interface ICajaService {
	public List<Caja> listarCajas(); //Listar All 
	
	public Caja guardarCaja(Caja caja);	//Guarda una CAJA CREATE
	
	public Caja cajaXID(String num_referencia); //Leer datos de una caja READ
	
	public Caja actualizarCaja(Caja caja); //Actualiza datos de la caja UPDATE
	
	public void eliminarCaja(String num_referencia);// Elimina la caja DELETE
}
