package com.UD251.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD251.dto.Almacen;

public interface IAlmacenDAO extends JpaRepository<Almacen, Long>{

}
