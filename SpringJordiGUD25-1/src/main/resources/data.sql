DROP table IF EXISTS `caja`;
DROP TABLE IF EXISTS `almacen`;



CREATE TABLE `almacen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `lugar` varchar(250) DEFAULT NULL,
  `capacidad` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `caja` (
  `num_referencia` char(10) NOT NULL,
  `contenido` varchar(100) DEFAULT NULL,
  `valor` int DEFAULT NULL,
  `id_almacen` int DEFAULT NULL,
  PRIMARY KEY (`num_referencia`),
  CONSTRAINT `caja_fk` FOREIGN KEY (`id_almacen`) REFERENCES `almacen` (`id`)
);


insert into almacen (lugar,capacidad)values('Constanti',100);
insert into almacen (lugar,capacidad)values('Valls',250);


insert into caja (num_referencia, contenido,valor, id_almacen) values ('1','Patos de goma',30,1);
insert into caja (num_referencia, contenido,valor, id_almacen) values ('2','Dinosaurio plastico',60,1);
insert into caja (num_referencia, contenido,valor, id_almacen) values ('3','boligrafo',1,2);
insert into caja (num_referencia, contenido,valor, id_almacen) values ('4','tipex',2,2);

