package com.UD251.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.UD251.dto.Sala;
import com.UD251.service.SalaServiceImpl;

@RestController
@RequestMapping("/api")
public class SalaController {
	@Autowired
	SalaServiceImpl salaSercideImpl; 
	
	@GetMapping("/salas")
	public List<Sala> listarSalas(){
		return salaSercideImpl.listarSala();
	}
	
	@PostMapping("/salas")
	public Sala salvarSala(@RequestBody Sala video) {
		
		return salaSercideImpl.guardarSala(video);
	}
	
	@GetMapping("/salas/{id}")
	public Sala salaXID(@PathVariable(name="id") Long id) {
		
		Sala sala_xid= new Sala();
		
		sala_xid=salaSercideImpl.salaXID(id);
		
		System.out.println("Sala XID: "+sala_xid);
		
		return sala_xid;
	}
	
	@PutMapping("/salas/{id}")
	public Sala actualizarSala(@PathVariable(name="id")Long id,@RequestBody Sala sala) {
		
		Sala sala_seleccionado= new Sala();
		Sala sala_actualizado= new Sala();
		
		sala_seleccionado= salaSercideImpl.salaXID(id);
		
		sala_seleccionado.setNombre(sala.getNombre());
		
		sala_actualizado = salaSercideImpl.actualizarSala(sala_seleccionado);
		
		System.out.println("La sala actualizada es: "+ sala_actualizado);
		
		return sala_actualizado;
	}
	
	@DeleteMapping("/salas/{id}")
	public void eliminarSala(@PathVariable(name="id")Long id) {
		salaSercideImpl.eliminarSala(id);
	}

}
