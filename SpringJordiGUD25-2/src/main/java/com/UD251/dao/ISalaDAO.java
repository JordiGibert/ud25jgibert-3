package com.UD251.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD251.dto.Sala;



public interface ISalaDAO extends JpaRepository<Sala, Long>{

}
