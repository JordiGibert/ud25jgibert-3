package com.UD251.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD251.dto.Pelicula;

public interface IPeliculaDAO extends JpaRepository<Pelicula, Long>{

}
