package com.UD251.service;

import java.util.List;

import com.UD251.dto.Pelicula;



public interface IPeliculaService {
	
	public List<Pelicula> listarPeliculas(); //Listar All 
	
	public Pelicula guardarPelicula(Pelicula pelicula);	//Guarda una pelicula CREATE
	
	public Pelicula peliculaXID(Long id); //Leer datos de una pelicula READ
	
	public Pelicula actualizarPelicula(Pelicula pelicula); //Actualiza datos de la pelicula UPDATE
	
	public void eliminarPelicula(Long id);// Elimina la pelicula DELETE
}
