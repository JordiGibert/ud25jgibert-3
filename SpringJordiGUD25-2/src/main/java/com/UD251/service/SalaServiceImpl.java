package com.UD251.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.UD251.dao.ISalaDAO;
import com.UD251.dto.Sala;

@Service
public class SalaServiceImpl implements ISalaService{

	@Autowired
	ISalaDAO iSalaDAO;
	
	@Override
	public List<Sala> listarSala() {
		// TODO Auto-generated method stub
		return iSalaDAO.findAll();
	}

	@Override
	public Sala guardarSala(Sala sala) {
		// TODO Auto-generated method stub
		return iSalaDAO.save(sala);
	}

	@Override
	public Sala salaXID(Long id) {
		// TODO Auto-generated method stub
		return iSalaDAO.findById(id).get();
	}

	@Override
	public Sala actualizarSala(Sala sala) {
		// TODO Auto-generated method stub
		return iSalaDAO.save(sala);
	}

	@Override
	public void eliminarSala(Long id) {
		// TODO Auto-generated method stub
		iSalaDAO.deleteById(id);
		
	}

}
