package com.UD251.service;

import java.util.List;

import com.UD251.dto.Sala;



public interface ISalaService {

	//Metodos del CRUD
			public List<Sala> listarSala(); //Listar All 
			
			public Sala guardarSala(Sala sala);	//Guarda una sala CREATE
			
			public Sala salaXID(Long id); //Leer datos de una sala READ
			
			public Sala actualizarSala(Sala sala); //Actualiza datos de la sala UPDATE
			
			public void eliminarSala(Long id);// Elimina la sala DELETE
}
