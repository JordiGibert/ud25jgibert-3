DROP table IF EXISTS `sala`;
DROP TABLE IF EXISTS `pelicula`;



CREATE TABLE `pelicula` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `calificacion_edad` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `sala` (
   `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `id_pelicula` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `sala_fk` FOREIGN KEY (`id_pelicula`) REFERENCES `pelicula` (`id`)
);


insert into pelicula (nombre,calificacion_edad)values('El ataque del mutante del espacio exterior',18);
insert into pelicula (nombre,calificacion_edad)values('Gatos vs Perros',7);


insert into sala (nombre, id_pelicula) values('Sala 1',1);
insert into sala (nombre, id_pelicula) values('Sala 2',2);
insert into sala (nombre, id_pelicula) values('Sala 3',1);
insert into sala (nombre, id_pelicula) values('Sala 4',2);

